package org.smart4j.demo;

import org.smart4j.framework.mvc.DataContext;
import org.smart4j.framework.util.CastUtil;


public class Tool {

    public static int getPageSize(String key) {
        return CastUtil.castInt(DataContext.Cookie.get("cookie.ps_" + key), 2);
    }

    public static String getBasePath() {
        return DataContext.getServletContext().getRealPath("") + Constant.UPLOAD_PATH;
    }
}
