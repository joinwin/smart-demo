package org.smart4j.demo;

import org.smart4j.framework.core.ConfigHelper;


public interface Constant {

	 String CAPTCHA = "session.captcha";

	 String PAGE_NUMBER = "pageNumber";
	 String PAGE_SIZE = "pageSize";

	 String UPLOAD_PATH = ConfigHelper.getConfigString("upload_path");
	 String TEMPLATE_PATH = ConfigHelper.getConfigString("template_path");

      String LOGIN_FAILURE = "login_failure";
}
