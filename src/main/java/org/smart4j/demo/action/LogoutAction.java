package org.smart4j.demo.action;

import org.smart4j.framework.mvc.annotation.Action;
import org.smart4j.framework.mvc.annotation.Request;
import org.smart4j.framework.mvc.bean.Result;
import org.smart4j.security.SmartSecurityHelper;



@Action
public class LogoutAction {

    @Request.Get("/logout")
    public Result logout() {
        // 获取当前用户并进行登出操作
        SmartSecurityHelper.logout();

        return new Result(true);
    }
}
