package org.smart4j.demo.action;
import org.smart4j.demo.Constant;
import org.smart4j.demo.Tool;
import org.smart4j.framework.dao.bean.Pager;
import org.smart4j.framework.mvc.annotation.Action;
import org.smart4j.framework.mvc.annotation.Request;
import org.smart4j.framework.mvc.bean.Params;
import org.smart4j.framework.mvc.bean.View;
import org.smart4j.plugin.search.SearchHelper;
import org.smart4j.plugin.search.bean.SearchResult;
@Action
public class SearchAction {		 
	@Request.Get("/search")
    public View search() {
		/*
		int pageNumber = 1;
	    int pageSize = Tool.getPageSize("search_pager");
	    String name = "iphone";
	    
	    Pager<SearchResult> searchResultPager=SearchHelper.search(name, pageNumber, pageSize);   
        return new View("search.jsp").data("searchResultPager", searchResultPager);
        
        */
		return new View("search.jsp");
    }
	 
    @Request.Post("/search")
    public View search(Params param) {
        int pageNumber = param.getInt(Constant.PAGE_NUMBER)==0?1:param.getInt(Constant.PAGE_NUMBER);
        int pageSize =param.getInt(Constant.PAGE_SIZE)==0?Tool.getPageSize("search_pager"):10;
        String name = param.getString("name");  
        Pager<SearchResult> searchResultPager =SearchHelper.search(name, pageNumber, pageSize);   
        return new View("search_list.jsp")
            .data("searchResultPager", searchResultPager);
    }  
}
