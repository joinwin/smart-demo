package org.smart4j.demo.action;

import java.util.HashMap;
import java.util.Map;

import org.smart4j.demo.Constant;
import org.smart4j.framework.mvc.annotation.Action;
import org.smart4j.framework.mvc.annotation.Request;
import org.smart4j.framework.mvc.bean.View;
import org.smart4j.plugin.template.impl.VelocityTemplateEngine;



@Action
public class SpaceAction {

    @Request.Get("/space")
    public View space() {
        return new View("space.jsp");
    }
    

    @Request.Get("/index")
    public View index() {
        return new View("index.jsp");
    }
    
    @Request.Get("/template")
    public void test(){
    	System.out.println("^^^^^^^^^^^^^^^^^^^^");
    	VelocityTemplateEngine vt=new VelocityTemplateEngine(Constant.TEMPLATE_PATH);
    	Map<String, Object> templateDataMap =new HashMap<String,Object>();
    	templateDataMap.put("name", "dreamhead");
    	String result=vt.mergeTemplateFile("hello.vm", templateDataMap);
    	System.out.println(result);
    	
    }
       
}
