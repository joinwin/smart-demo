package org.smart4j.demo.service;
public interface LogService {

    void log(String description);
}