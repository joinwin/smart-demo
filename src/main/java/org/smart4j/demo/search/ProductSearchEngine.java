package org.smart4j.demo.search;



import java.util.List;

import org.apache.lucene.document.Document;
import org.smart4j.demo.entity.Product;
import org.smart4j.framework.dao.DataSet;
import org.smart4j.framework.ioc.annotation.Bean;
import org.smart4j.framework.util.CollectionUtil;
import org.smart4j.plugin.search.IndexFieldName;
import org.smart4j.plugin.search.SearchEngine;
import org.smart4j.plugin.search.bean.IndexData;
import org.smart4j.plugin.search.bean.IndexDocument;
import org.smart4j.plugin.search.bean.IndexField;
import org.smart4j.plugin.search.bean.SearchResult;

@Bean
public class ProductSearchEngine  implements SearchEngine {

    @Override
    public IndexData createIndexData() {
        IndexData indexData = new IndexData();
        List<Product> productList = DataSet.selectList(Product.class, "", "");
        if (CollectionUtil.isNotEmpty(productList)) {
            for (Product product : productList) {
                IndexDocument indexDocument = new IndexDocument();
                indexDocument.addIndexField(new IndexField(IndexFieldName.title, product.getName()));
                indexDocument.addIndexField(new IndexField(IndexFieldName.content, product.getDescription()));
                indexData.addIndexDocument(indexDocument);
            }
        }
        return indexData;
    }

    @Override
    public SearchResult createSearchResult(Document document) {
        SearchResult searchResult = new SearchResult();
        searchResult.setTitle(document.get("title"));
        searchResult.setContent(document.get("content"));
        return searchResult;
    }
}
