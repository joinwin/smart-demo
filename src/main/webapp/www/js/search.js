$(function() {
    var pager = new Smart.Pager(
        'search_pager',
        function (pageNumber) {
            searchFormData.pageNumber = pageNumber;
            $('#search_form').submit();
        },
        function(pageSize) {
            searchFormData.pageNumber = 1;
            searchFormData.pageSize = pageSize;
            $('#search_form').submit();
        }
    );

    var searchFormData = {
        'pageNumber': pager.getPageNumber(),
        'pageSize': pager.getPageSize()
    };

    $('#search_form').ajaxForm({
        type: 'post',
        url: BASE + '/search',
        data: searchFormData,
        dataType: 'html',
        beforeSubmit: function() {
            return Smart.Validator.checkRequired('search_form');
        },
        success: function(html) {
            $('#search_list').html(html);
        }
    });

  
});