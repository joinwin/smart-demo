<%@ page pageEncoding="UTF-8"%>
<%@ include file="common/global.jsp"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="common/meta.jsp"%>
<title><f:message key="common.title" /> - <f:message
		key="product" /></title>
<%@ include file="common/css.jsp"%>
</head>
<body>
	<%@ include file="common/header.jsp"%>

	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					<f:message key="product.new_product" />
				</div>
				<div class="panel-body">
					<form role="form" id="product_create_form"
						enctype="multipart/form-data">
						<div class="form-group">
							<label for="productType" class="control-label"><f:message
									key="product.product_type" />:</label> <select id="productType"
								name="productTypeId" class="ext-required">
								<c:forEach var="productType" items="${productTypeList}">
									<option value="${productType.id}">${productType.name}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label for="name" class="control-label"><f:message
									key="product.name" />:</label> <input type="text" id="name"
								name="name" class="ext-required"> *
						</div>
						<div class="form-group">
							<label for="code" class="control-label"><f:message
									key="product.code" />:</label> <input type="text" id="code"
								name="code" class="ext-required"> <span
								class="css-color-red">*</span>
						</div>
						<div class="form-group">
							<label for="price" class="control-label"><f:message
									key="product.price" />:</label> <input type="text" id="price"
								name="price" class="ext-required"> <span
								class="css-color-red">*</span>
						</div>
						<div class="form-group">
							<label for="description" class="control-label"><f:message
									key="product.description" />:</label>
							<textarea id="description" name="description" rows="5"></textarea>
						</div>
						<div class="form-group">
							<label for="picture" class="control-label"><f:message
									key="product.picture" />:</label> <input type="file" id="picture"
								name="picture">
						</div>

						<button type="submit" class="btn btn-default">
							<f:message key="common.save" />
						</button>
						<button type="button" id="back" class="btn btn-default">
							<f:message key="common.back" />
						</button>
					</form>
				</div>
			</div>
			<!-- end of panel -->
		</div>
	</div>

	<%@ include file="common/footer.jsp"%>
	<%@ include file="common/js.jsp"%>
	<script type="text/javascript" src="${BASE}/www/js/product_create.js"></script>
</body>
</html>