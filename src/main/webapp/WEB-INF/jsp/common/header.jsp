<%@ page pageEncoding="UTF-8" %>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="${BASE}" class="navbar-brand"><f:message key="common.title"/>	</a>
                        
    </div>
    <div class="collapse navbar-collapse" id="navbar">
        <ul class="nav navbar-nav">
        	<li><a href="#"><f:message key="login.username"/>：<security:principal/></a></li>
        	<li class="dropdown">
                <a href="" class="dropdown-toggle" data-toggle="dropdown"><f:message key="common.role"/> <b class="caret"></b></a>
                
                <ul class="dropdown-menu">
                 <security:hasAnyRoles name="admin,user">					 
			        <security:hasRole name="admin">
			            <li><a href="#">管理员</a></li>
			        </security:hasRole>
			        <security:hasRole name="user">
			            <li><a href="#">普通用户</a></li>
			        </security:hasRole>
				  </security:hasAnyRoles>
                </ul>
               
            </li>          
        </ul>
        
        <ul class="nav navbar-nav ">        	
            <li class="dropdown">
                <a href="" class="dropdown-toggle" data-toggle="dropdown"><f:message key="common.permission"/> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                <security:hasAnyPerms name="product.view,product.new,product.edit,product.delete">
                    <security:hasPerm name="product.view">
		            	<li><a href="<c:url value="/product"/>"><f:message key="product.view_product" /></a></li>
			        </security:hasPerm>
			        <security:hasPerm name="product.new">
			            <li><a href="<c:url value="/product/create"/>"><f:message key="product.new_product" /></a></li>
			        </security:hasPerm>
			        <security:hasPerm name="product.edit">
			            <li><a href="#">编辑产品</a></li>
			        </security:hasPerm>
			        <security:hasPerm name="product.delete">
			            <li><a href="#">删除产品</a></li>
			        </security:hasPerm>
			      </security:hasAnyPerms>
                </ul>
            </li>                        
        </ul>
        
        <ul class="nav navbar-nav navbar-right">
        <li><a href="#" id="logout"><f:message key="common.logout"/></a></li>
        </ul>
    </div>
</nav>