<%@ page pageEncoding="UTF-8" %>
<%@ include file="common/global.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <%@ include file="common/meta.jsp" %>
    <title><f:message key="common.title"/> - <f:message key="welcome"/></title>
    <%@ include file="common/css.jsp" %>
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
        <div class="navbar-brand"><f:message key="common.title"/></div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading"><f:message key="register"/></div>
                <div class="panel-body">
				<security:guest>
				    <form action="<c:url value="/register"/>" method="post" class="form-horizontal">
				         <div class="form-group">
                            <label for="username" class="col-sm-4 control-label text-right"><f:message key="login.username"/>:</label>
                            <div class="col-sm-6">
                                <input type="text" name="username" class="form-control">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="password" class="col-sm-4 control-label text-right"><f:message key="login.password"/>:</label>
                            <div class="col-sm-6">
                                <input type="password" name="password" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="password" class="col-sm-4 control-label text-right"><f:message key="login.password"/>:</label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control">
                            </div>
                        </div>
                        
                         <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-4 text-right">
                                <button type="submit" class="btn btn-default btn-success btn-lg"><f:message key="register"/></button>
                            </div>
                        </div>
				        
				    </form>
				    <c:if test="${requestScope['exception'] == 'RegisterException'}">
				        <p>注册失败：用户已存在！</p>
				    </c:if>
				</security:guest>
				
				<security:user>
				    <c:redirect url="/space"/>
				</security:user>

				</div>
            </div>
            <!-- Text Under Box -->
			<div class="login-extra">
				<f:message key="register.have_account" />? <a href="<c:url value="/login" />"><f:message key="login" /></a>
			</div> <!-- /login-extra -->
        </div>
        
    </div>
      
</div>

<%@ include file="common/footer.jsp" %>
<%@ include file="common/js.jsp" %>
</body>
</html>