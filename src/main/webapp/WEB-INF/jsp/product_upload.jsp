<%@ page pageEncoding="UTF-8" %>
<%@ include file="common/global.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <%@ include file="common/meta.jsp" %>
    <title><f:message key="common.title"/> - <f:message key="product"/></title>
    <%@ include file="common/css.jsp" %>
</head>
<body>

<%@ include file="common/header.jsp" %>

<div class="container">
 <div class="row">
      <ol class="breadcrumb">
		  <li><a href="#">Home</a></li>
		  <li class="active"><f:message key="common.upload"/></li>
		</ol>
      </div>
    <div class="row">
        <form id="product_upload_form" enctype="multipart/form-data" role="form">             
            <div class="form-group">
            	<input type="hidden" id="id" value="${product.id}">
            </div>
           
            <div class="form-group">
                <label for="picture"><f:message key="product.picture"/>:</label>
                <c:set var="picture" value="www/upload/${product.picture}"/>
                <img id="picture_img" src="${BASE}/${not empty product.picture ? picture : 'www/img/s.gif'}" height="128"/>
            </div>
            <div class="form-group">
                <label></label>
                <input type="file" id="picture" name="picture">
            </div>
            <div class="form-group">
           	   <label></label>
                <button class="btn btn-default" type="button" id="back"><f:message key="common.back"/></button>
            </div>
        </form>
    </div>
</div>

<%@ include file="common/footer.jsp" %>

<%@ include file="common/js.jsp" %>

<script type="text/javascript" src="${BASE}/www/js/product_upload.js"></script>

</body>
</html>