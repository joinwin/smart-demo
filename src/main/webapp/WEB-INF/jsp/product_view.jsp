<%@ page pageEncoding="UTF-8"%>
<%@ include file="common/global.jsp"%>

<c:set var="product" value="${productBean.product}" />
<c:set var="productType" value="${productBean.productType}" />

<!DOCTYPE html>
<html>
<head>
<%@ include file="common/meta.jsp"%>
<title><f:message key="common.title" /> - <f:message
		key="product" /></title>
<%@ include file="common/css.jsp"%>
</head>
<body>

	<%@ include file="common/header.jsp"%>


	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					<f:message key="product.view_product" />
				</div>
				<div class="panel-body">
					<form role="form">
						<div class="col-md-6">
							<div class="form-group">
								<input type="hidden" id="id" value="${product.id}" />
							</div>
							<div class="form-group">
								<label><f:message key="product.product_type" />:</label> <input
									type="text" value="${productType.name}" class="css-readonly"
									readonly>
							</div>
							<div class="form-group">
								<label><f:message key="product.name" />:</label> <input
									type="text" value="${product.name}" class="css-readonly"
									readonly>
							</div>
							<div class="form-group">
								<label><f:message key="product.code" />:</label> <input
									type="text" value="${product.code}" class="css-readonly"
									readonly>
							</div>
							<div class="form-group">
								<label><f:message key="product.price" />:</label> <input
									type="text" value="${product.price}" class="css-readonly"
									readonly>
							</div>
							<div class="form-group">
								<label><f:message key="product.description" />:</label>
								<textarea rows="5" class="css-readonly" readonly>${product.description}</textarea>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label><f:message key="product.picture" />:</label>
								<c:set var="picture" value="www/upload/${product.picture}" />
								<img
									src="${BASE}/${not empty product.picture ? picture : 'www/img/s.gif'}"
									height="128" />
							</div>
							<div class="form-group">
								<label></label> <a
									href="${BASE}/product/upload_picture/${product.id}"><f:message
										key="common.upload" /></a> <a
									href="${BASE}/product/download_picture/${product.id}"><f:message
										key="common.download" /></a>
							</div>
						</div>
						<div class="form-group">
							<button type="button" id="edit">
								<f:message key="common.edit" />
							</button>
							<button type="button" id="back">
								<f:message key="common.back" />
							</button>
						</div>
					</form>
				</div>
			</div>
			<!-- end of panel -->
		</div>
	</div>
	<!-- /container -->



	<%@ include file="common/footer.jsp"%>

	<%@ include file="common/js.jsp"%>

	<script type="text/javascript" src="${BASE}/www/js/product_view.js"></script>

</body>
</html>