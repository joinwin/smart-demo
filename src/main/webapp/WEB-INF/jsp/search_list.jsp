<%@ page pageEncoding="UTF-8"%>
<%@ include file="common/global.jsp"%>

<c:set var="searchResultList" value="${searchResultPager.recordList}" />
<div class="panel panel-default">
	<!-- Default panel contents -->
	<div class="panel-heading">
		<f:message key="product.product_list" />
	</div>
	<!-- Table -->
	<table class="table">
		<thead>
			<tr>
				<td><f:message key="product.name" /></td>
				<td><f:message key="product.description" /></td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="searchResult" items="${searchResultList}">
				<tr>
					<td>${searchResult.title}</td>
					<td>${searchResult.content}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<tag:pager2 id="search_pager" pager="${searchResultPager}" />