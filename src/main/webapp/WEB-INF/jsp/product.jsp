<%@ page pageEncoding="UTF-8"%>
<%@ include file="common/global.jsp"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="common/meta.jsp"%>
<title><f:message key="common.title" /> - <f:message
		key="product" /></title>
<%@ include file="common/css.jsp"%>
</head>
<body>
<%@ include file="common/header.jsp"%>

<div class="container">


	<nav class="navbar navbar-default" role="navigation">			
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-2">
			<form id="product_search_form" class="navbar-form navbar-left" role="search">
				<div class="form-group">
					<input name="name" type="text" class="form-control ext-required"
						placeholder="<f:message key="product.name"/>">
				</div>
				<button type="submit" class="btn btn-default">
					<f:message key="common.search" />
				</button>
			</form>
			<a class="navbar-brand navbar-right btn" href="${BASE}/product/create"><f:message key="product.new_product" /></a>
		</div>			
	</nav>

	<div id="product_list">
			<%@ include file="product_list.jsp"%>
	</div><!-- end of product_list  -->
		
</div>
<%@ include file="common/footer.jsp"%>
<%@ include file="common/js.jsp"%>
<script type="text/javascript" src="${BASE}/www/js/product.js"></script>
</body>
</html>