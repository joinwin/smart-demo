<%@ page pageEncoding="UTF-8"%>
<%@ include file="common/global.jsp"%>

<!DOCTYPE html>
<html>
<head>
<%@ include file="common/meta.jsp"%>
<title><f:message key="common.title" /> - <f:message
		key="product" /></title>
<%@ include file="common/css.jsp"%>
</head>
<body>
<%@ include file="common/header.jsp"%>

<div class="container">


	
	<nav class="navbar navbar-default" role="navigation">			
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-2">
			<form id="search_form" class="navbar-form navbar-left" role="search">
				<div class="form-group">
					<input name="name" type="text" class="form-control ext-required"
						placeholder="<f:message key="product.name"/>">
				</div>
				<button type="submit" class="btn btn-default">
					<f:message key="common.search" />
				</button>
			</form>
			
		</div>			
	</nav>

	<div id="search_list">
	
	</div><!-- end of search_list  -->
		
</div>
<%@ include file="common/footer.jsp"%>
<%@ include file="common/js.jsp"%>
<script type="text/javascript" src="${BASE}/www/js/search.js"></script>
</body>
</html>